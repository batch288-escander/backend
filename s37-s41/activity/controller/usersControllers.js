const Users = require('../models/Users.js');
const Courses = require('../models/Courses.js');
const bcrypt = require('bcrypt')
const auth = require('../auth.js')

module.exports.registerUser = (request, response) => {
    Users.findOne({email: request.body.email})
        .then(result => {
            if(result) {
                response.send(`${request.body.email} has been taken! Try logging in or use different email in signing up!`)
            } else {
                let newUser = new Users({
                    firstName: request.body.firstName,
                    lastName: request.body.lastName,
                    email: request.body.email,
                    password: bcrypt.hashSync(request.body.password,10),
                    isAdmin: request.body.isAdmin,
                    mobileNo: request.body.mobileNo,
                })

                newUser.save()
                    .then(saved => response.send(true))
                    .catch(error => response.send(false))
            }
        }).catch(error => response.send(false))
}

module.exports.loginUser = (request, response) => {
    Users.findOne({email: request.body.email})
    .then(result => {
        if(!result){
            return response.send(false)
        } else {
            const isPasswordCorrect = bcrypt.compareSync(request.body.password, result.password);

            if(isPasswordCorrect) {
                return response.send({auth:auth.createAccessToken(result)})
            } else {
                return response.send(false)
            }
        }
    }).catch(error => response.send(false))

}

module.exports.getProfile = ( request, response ) => {
    const userData = auth.decode(request.headers.authorization)
    if(userData.isAdmin) {
        Users.findById(request.body.id)
        .then(result => {
            if(!result){
                return response.send(false);
            } else {
                result.password =""
                return response.send(result)
            }
        }).catch(error => response.send(false))
    } else {
        return response.send(false);
    }
}


module.exports.enrollCourse = (request, response) => {
    const userData = auth.decode(request.headers.authorization);
    if(!userData.isAdmin){
        let courseId = request.body.id

        let isUsersSaved = Users.findOne({ 
            _id: userData.id
        }).then(result => {
            result.enrollments.push({courseId: courseId});
            result.save()
            .then(() => true)
            .error(() => false)
        })
        .catch(error => false)

        
        let isCoursesSaved = Courses.findOne({ 
            _id: courseId
        }).then(result => {
            result.enrollees.push({userId:userData.id});
            result.save()
            .then(() => true)
            .error(() => false)
        })
        .catch(error => false)

        if (isUsersSaved && isCoursesSaved){
            return response.send(true)
        } else {
            return response.send(false)
        }
    } else {
        return response.send(false)
    }
}

module.exports.retrieveUserDetails = (request, response) => {
    const userData =auth.decode(request.headers.authorization);

    Users.findById(userData.id)
    .then(data => response.send(data))
    .catch(() => response.send(false))
}