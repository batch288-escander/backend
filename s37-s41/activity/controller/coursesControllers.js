const Courses = require('../models/Courses.js')
const auth = require('../auth.js')

module.exports.addCourse = (request, response) => {
    const userData = auth.decode(request.headers.authorization)
    if(userData.isAdmin) {
        let newCourse = new Courses({
            name: request.body.name,
            description: request.body.description,
            price: request.body.price,
            isActive: request.body.isActive,
            slots: request.body.slots
        });
        newCourse.save()
        .then(result => response.send(true))
        .catch(error => response.send(false))
    } else {
        response.send(false)
    }
}

module.exports.getAllCourses = (request, response) => {
    Courses.find()
        .then(results => response.send(results))
        .catch(error => response.send(false))
}

module.exports.getActiveCourses = (request, response) => {
    Courses.find({ isActive: true })
        .then(results => response.send(results))
        .catch(error => response.send(false))
}

module.exports.getCourse = (request, response) => {
    Courses.findById(request.params.courseId)
        .then(result => response.send(result))
        .catch(error => response.send(false))
}

module.exports.updateCourse = (request, response) => {
    const userData = auth.decode(request.headers.authorization);
    const courseId = request.params.courseId;

    let updatedCourse = {
        name: request.body.name,
        description: request.body.description,
        price: request.body.price,
    }

    if(userData.isAdmin){
        Courses.findByIdAndUpdate(courseId, updatedCourse)
        .then(() => response.send(true))
        .catch(error => response.send(false))
    }else{
        return response.send(false)
    }
}

module.exports.archiveCourse = (request, response) => {
    const userData = auth.decode(request.headers.authorization);
    const courseId = request.params.courseId;
    
    if(userData.isAdmin){
        Courses.findByIdAndUpdate(courseId, {
            isActive: false
        })
        .then(() => response.send(true))
        .catch(error => response.send(false))
    }else{
        return response.send(false)
    }
}


module.exports.getInactiveCourses = (request, response) => {
    const userData = auth.decode(request.headers.authorization);
    
    if(userData.isAdmin){
        Courses.find({ isActive: false })
        .then(results => response.send(results))
        .catch(error => response.send(false))
    } else {
        return response.send(false)
    }
}

