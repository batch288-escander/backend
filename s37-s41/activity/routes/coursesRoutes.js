const express = require('express');

const coursesControllers = require('../controller/coursesControllers.js')
const auth = require('../auth.js')

const router = express.Router()

//Routes
router.post('/addCourse', [auth.verify, coursesControllers.addCourse])
router.get('/', coursesControllers.getAllCourses)
router.get('/active', coursesControllers.getActiveCourses)
router.get('/:courseId', coursesControllers.getCourse)
router.patch('/:courseId', [auth.verify, coursesControllers.updateCourse])
router.patch('/:courseId/archive', [auth.verify, coursesControllers.archiveCourse])


module.exports = router;