const express = require('express');

const usersControllers = require('../controller/usersControllers.js')
const auth = require('../auth.js')

const router = express.Router()

//Routes
router.post('/register', usersControllers.registerUser )
router.post('/login', usersControllers.loginUser)
router.get('/details', [auth.verify, usersControllers.getProfile])
router.get('/userDetails', [auth.verify, usersControllers.retrieveUserDetails])
router.post('/enroll', [auth.verify, usersControllers.enrollCourse])


module.exports = router;