const mongoose = require('mongoose');


const userSchema = new mongoose.Schema({
    firstName: {
        type: String,
        required: [true, "First name is requried!"]
    },

    lastName: {
        type: String,
        required: [true, "Last name is requried!"]
    },

    email: {
        type: String,
        required: [true, "Email is requried!"]
    },
    
    password: {
        type: String,
        required: [true, "Password is requried!"]
    },
    
    isAdmin: {
        type: Boolean,
        default: false
    },
    
    mobileNo: {
        type: String,
        required: [true, "Mobile No. is requried!"]
    },

    enrollments: [
        {
            courseId: {
                type: String,
                required: [true, "Course ID of the enrolled course is required!"]
            },
            
            enrolledOn: {
                type: Date,
                default: new Date
            },

            status: {
                type: String,
                default: "Enrolled"
            }
        }
    ]
})

const Users = mongoose.model("Users", userSchema);

module.exports = Users;