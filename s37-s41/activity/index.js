const express = require('express');
const mongoose = require('mongoose');
const usersRoutes = require('./routes/usersRoutes.js')
const coursesRoutes = require('./routes/coursesRoutes.js')

//It will allow our backend application to be available to our frontend application
//It will also allows us to control the app's Cross Origin Resource Sharing settings.
const cors = require('cors');

const port = 4001;

const app = express();


//Mongod DB connection
// Establish the connection between the DB and the application or server.
//THe name of the databse should be :"CourseBookingAPI"
		mongoose.connect("mongodb+srv://admin:admin@batch288escander.nhk1xgq.mongodb.net/CourseBookingAPI?retryWrites=true&w=majority", {
			useNewUrlParser:true,
			useUnifiedTopology: true
		})

	let db = mongoose.connection;

	db.on("error", console.error.bind(console, "Network problem, can't connect to the db!"))

	db.once("open", ()=>console.log('Connected to the cloud database!'))
//Middlewares
app.use(express.json());
app.use(express.urlencoded({extended: true}));
app.use(cors());
app.use('/users', usersRoutes);
app.use('/courses', coursesRoutes);

if(require.main === module){
	app.listen(process.env.PORT || 4000, () => {
	    console.log(`API is now online on port ${ process.env.PORT || 4000 }`)
	});
}

module.exports = {app,mongoose};